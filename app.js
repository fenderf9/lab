const express = require('express');
const app = express();
const port = 8080;
const fs = require ("fs");
const path = require ("path");

const dir = `./storage`;

try{
    if(!fs.existsSync(dir)){
        fs.mkdirSync(dir);
        console.log(`directory was created`)
    }
    console.log(`directory was already created`);
}
catch(err){
    console.log(err);
}

const passwordFile = `./passwords.json`

try{
    if(!fs.existsSync(passwordFile)){
        fs.writeFileSync(passwordFile, `{}`);
        console.log(`file was created`)
    }
    console.log(`file was already created`);
}
catch(err){
    console.log(err);
}

function createPassword(password,fileName){
    fs.readFile(passwordFile,`utf8`,(err,data) => {
        if(err){
             console.error(err.stack);
             return false
        }
        const obj = JSON.parse(data);
        if(password === undefined){
            obj[fileName] = null;
        }else{
            obj[fileName] = password;
        }
        obj[fileName] = password;
        fs.writeFile(passwordFile,JSON.stringify(obj),err =>{
            if (err) {
                console.error(err.stack);
                return false
            }
            console.log(`password created`)
            return true;
        })
    })
}

function checkPassword(password,filename,res,execFunc){
    fs.readFile(passwordFile,`utf8`,(err,data) => {
        if(err){
             console.error(err.stack);
             return false
        }
        const obj = JSON.parse(data);
        if (obj[filename] === password || password === undefined && obj[filename] === null){
                execFunc()
                return
        }
        else{
            res.status(403).send({"message": "need a password/incorrect password"});  
        }
      
    })    
}

function isAcceptableFormat(filename){
    try{
        let index = -1;
        for(let i = filename.length - 1; i >= 0; i--){
            if (filename[i] === `.`){
                index = i;
                break;
            }
        }
        if(index === -1){
            return null
        }
    let substr = filename.slice(index + 1,filename.length);
    if(
        substr === `log` ||
        substr === `txt` ||
        substr === `json` ||
        substr === `yaml` ||
        substr === `xml` ||
        substr === `js` 
        ) {
            return substr;
        }
        else{
            return null;
        }
    }
    catch(err){
        console.log(`${typeof filename} not an apropriate extention`)
        return null
    }
}

const createChangeFile = function (req,res) {
    const fileFormat = isAcceptableFormat(req.body.filename);
    
    function writeInFile(){
        fs.writeFile(`${dir}/${req.body.filename}`,req.body.content,err =>{
            if (err) {
                return res.status(500).send({"message": `${err}`});
            }
            res.set({'Content-Type':'application/json'});
            res.status(200).send({"message": "File created successfully"});
        })
    }

    if(fileFormat){
        fs.access(path.join(dir,req.body.filename), fs.F_OK, (err) => {
            if (err) {
                if(req.method === `POST`){
                    createPassword(req.query.password,req.body.filename);
                    writeInFile();
                }
                else if(req.method === "PUT"){
                    return res.status(404).send({"message": "file does not exist"});
                }
            } 
            else {
                if(req.method === `POST`){
                    return res.status(409).send({"message": "file already exist"});
                }
                else if(req.method === "PUT"){
                    checkPassword(req.query.password,req.body.filename,res,writeInFile)
                }
            }
        })
    }
    else{
        res.status(400).send({"message": "Please specify 'content' parameter"});
    }
}

app.use(express.json());
app.use(`*`,(req,res,next) => {
    console.log(req.method);
    console.log(req.originalUrl)
    console.log(req.headers);
    console.log(req.body);
    next();
})

app.post(`/api/files`,(req,res) => {createChangeFile(req,res)});
app.put(`/api/files`,(req,res) => {createChangeFile(req,res)})

app.get(`/api/files`,(req,res) => {
    fs.readdir(dir,(err,files) =>{
        if (err){
            return res.status(500).send({"message": `${err}`});
        }
       
        res.status(200).send({
            "message": "Success!",
            "files": files
        });
    })
})

app.get(`/api/files/:filename`,(req,res) => {
    let downloadPath = path.join(dir,req.params.filename);
    checkPassword(req.query.password,req.params.filename,res,()=>{
        fs.readFile(downloadPath,`utf8`,(err,data) => {
            if(err){
                return res.status(400).send({"message": `No file with ${req.params.filename} filename found`});
            }
            if(!data){
                return res.status(500).send({"message": "server error"});
            }
            let ext = path.extname(downloadPath);

            res.status(200).send({
                "message": "Success",
                "filename": req.params.filename,
                "content": data,
                "extension": ext.slice(1,ext.length),
                "uploadedDate": fs.statSync(downloadPath).birthtime
            })
        })
    })
    
})

app.delete(`/api/files/:filename`,(req,res) => {
    let delPath = path.join(dir,req.params.filename);
    checkPassword(req.query.password,req.params.filename,res,()=>{
        fs.unlink(delPath,(err) => {
            if(err && err.code == 'ENOENT') {
                res.status(404).send({"message": "file does not exist"})
            } else if (err) {
                res.status(400).send({"message":"Error occurred while trying to remove file"});
            } else {
                res.status(200).send({"message":"removed"})
            }
        })
    })
})

app.use(`*`,(req,res) => {
    res.status(400).send({
        "message":"server does not support this type of command"});
});

app.listen(port,()=>{
    console.log(`server is running on port ${port}`);
});